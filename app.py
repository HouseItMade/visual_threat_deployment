import io
import os
import sys
import numpy as np
import flask
import skimage
import urllib.request
from flask import request, render_template
from wtforms import Form
import matplotlib
import matplotlib.pyplot as plt
from utils import create_paths, read_paths

# Import Mask RCNN
# Root directory of the project
ROOT_DIR = os.path.abspath(".")
sys.path.append(ROOT_DIR)  # To find local version of the library

from mrcnn.config import Config
from mrcnn import utils
import mrcnn.model as modellib
from mrcnn import visualize
from mrcnn.model import log


app = flask.Flask(__name__,
                    static_url_path="/static"
                    )
app.config.from_object(__name__)

def index():
    return flask.render_template("index.html")

class ReusableForm(Form):

    @app.route("/", methods=["GET"])
    def serve_form():
        return render_template("form.html")

    @app.route("/", methods=['POST'])
    def result():
        # url to download image from
        url = request.form["url"]
        # set up paths each time an image is given
        create_paths()
        DOWNLOADED_PATH, PREDICTED_PATH = read_paths()

        # needed to prevent error when reloading, or going back
        from keras import backend as K
        K.clear_session()


        if request.method =='POST':
            url = str(request.form["url"])
            model = load_model()
            urllib.request.urlretrieve(url, DOWNLOADED_PATH)
            image = skimage.io.imread(DOWNLOADED_PATH)

            # Run detection
            results = model.detect([image], verbose=2)

            # save results
            r = results[0]
            visualize.display_instances(image, r['rois'], r['masks'], r['class_ids'], 
                                        ["BG", "guns"], r['scores'])

            return render_template("result.html", url=url, filename=PREDICTED_PATH)

    # Clean up after saving image from url and displaying predicted image
    @app.after_request
    def remove_file(response):
        _, PREDICTED_PATH = read_paths()
        try:
            # remove the previously saved predicted images
            old_files = ["./static/{}".format(x) for x in os.listdir("./static/")]
            for old_file in old_files:
                if old_file != "./static/place_holder.txt" and old_file != PREDICTED_PATH:
                    os.remove(old_file)
        except Exception as error:
            print("Error removed predicted file(s)", error)
        return response


def load_model():

    class GunsConfig(Config):
        """Configuration for training on the toy  dataset.
        Derives from the base Config class and overrides some values.
        """
        # Give the configuration a recognizable name
        NAME = "guns"

        IMAGES_PER_GPU = 1

        # Number of classes (including background)
        NUM_CLASSES = 1 + 1  # Background + guns

        # Number of training steps per epoch
        STEPS_PER_EPOCH = 100

        # Skip detections with < 80% confidence
        DETECTION_MIN_CONFIDENCE = 0.8

    class InferenceConfig(GunsConfig):
        GPU_COUNT = 1
        IMAGES_PER_GPU = 1

    inference_config = InferenceConfig()

    MODEL_DIR = os.path.join(ROOT_DIR, "logs")
    model_path = ROOT_DIR + "/mask_rcnn_guns_resnet_50_reduced_params.h5"
    # Recreate the model in inference mode
    model = modellib.MaskRCNN(mode="inference", 
                            config=inference_config,
                            model_dir=MODEL_DIR)


    # Load trained weights
    print("Loading weights from ", model_path)
    model.load_weights(model_path, by_name=True)

    return model


if __name__ == "__main__":
    port = int(os.environ.get("PORT", 5000))
    app.run(host="0.0.0.0", port=port)
