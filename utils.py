import random
import json

def create_paths():
    # Paths to images
    downloaded_path = "downloaded_image.jpg"
    predicted_path = "./static/predicted_image_{}.png".format(
        random.randint(1,10**7)
    )

    paths = {"downloaded_path":downloaded_path,
            "predicted_path":predicted_path}
    
    with open("path_config.json", "w") as outfile:
        json.dump(paths, outfile)

def read_paths():
    global DOWNLOADED_PATH
    global PREDICTED_PATH

    with open("path_config.json", "r") as infile:
        paths = json.load(infile)
    DOWNLOADED_PATH = paths["downloaded_path"]
    PREDICTED_PATH = paths["predicted_path"]

    return DOWNLOADED_PATH, PREDICTED_PATH
